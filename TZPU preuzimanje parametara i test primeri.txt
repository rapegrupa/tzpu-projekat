-------->>>> STA MI SVE TREBA <<<<---------
1. Neka funkcija koja ce da izdvoji pojedinacne sablone (<p0>, <p1>, ... <pn>). Sabloni mogu da budu varijacije, kombinacije, i ne znam sta je jos navedeno.
2. Nakon sto izdvojim sablone, trebam da krenem sa strukturama, tipa onaj <for> </for> da parsira,
onda parsiranje uslovnih parametara, i jos neke stvari.
3. Ako resim izdvajanje pojedinacnih sablona, i ovo parsiranje, sredio sam solidnu kolicinu posla

4. Mozda cu morati da se nadjem sa Mladenom kako bi zajedno nesto radili, zavisi, videcu sta Pera misli

--<for> </for> je oko 25. strane u  dokumentu

--- viticaste zagrade oznacavaju tzv set parametara
---kombinacije[] su razdvojene zarezom bez ikakvog razmaka


--->> atribut this.parametri varijacije dodaje "cele", a elemente kombinacije kao jedan element, sto i odgovara prici da su to zapravo promenljivi parametri. Dakle, funkcija koja ce da obradi konkretnu varijaciju ili kombinaciju mora da vrati na taj nacin parametre, a mora i da vrati duzinu stringa koji je izdvojen, kako bi se nastavilo sa daljom analizom teksta... Znaci na prvi pogled dve stvari iz jedne funkcije, da vidim sad dal moze da se to nekako promeni, da on "sam" hvata pojavljivanja simbola

{(najveca)(drzava)[Evropi,Aziji,Africi]}

//--->> nista, sutra samo da bolje naucim operacije sa stringovima u C#, to cini kod dosta boljim
//--->> valjda cu se setim sutra da trebam da radim "destruktivnu obradu" stringa iz pomocne promenljive
//--->> sablon za .. i ,: Koja  je vrednost promenljive x? x = (<p0> > <p1>)? <p0> : <p1>
{(0..9,2)(5..13,3)}


Koja je <p0> <p1> u <p2>? //radi raznolikosti testiranja, pokusacu i <p0>[1] npr ili nesto tako
{(najveca)(drzava)[Evropi,Aziji,Africi]}{(najduza)(reka)}{(najvisa)(planina)}

--->>>indeksne zajebancije: <p2> <p2>[0] <p2>[<p2>.count] <p2>[<p2>.count-2]

Koja je najveca drzava po <p0> u <p1>?
{[povrsini,broju stanovnika][Aziji,Juznoj Americi]}
Rusija <if> <p0>:=povrsiniAND<p1>:=Aziji
Brazil <if> <p0>:=[povrsini,broju stanovnika]AND<p1>:=Juznoj Americi
Kina <if> <p0>:=[broju stanovnika]AND<p1>:=Aziji