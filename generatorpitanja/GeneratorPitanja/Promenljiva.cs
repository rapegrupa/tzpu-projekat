﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorPitanja
{
    public class Promenljiva
    {
        public int IdUGlavnojListi { get; set; }//ovo je pojavni redni broj za parametar
        public int DonjaGranica { get; set; } 
        public int GornjaGranica { get; set; }   
        public int Korak { get; set; }

        public Promenljiva()
        {
            
        }
        public Promenljiva(int IdUGlavnojListi, int DonjaGranica, int GornjaGranica, int Korak)
        {
            this.IdUGlavnojListi = IdUGlavnojListi;
            this.DonjaGranica = DonjaGranica;
            this.GornjaGranica = GornjaGranica;
            this.Korak = Korak;
        }
    }
}
