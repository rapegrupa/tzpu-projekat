﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorPitanja
{
    public class Permutiranje
    {
        private static void Permute(string[] res, int ix, List<List<string>> data,List<List<string>> izlaz) //ovo data mogu da preimenujem u nizNizovaParametara (za sada tako, posle nesto lepse)
        {
            foreach (string v in data[ix])
            {
                res[ix] = v;
                if (ix >= data.Count - 1)
                {
                    izlaz.Add(new List<string>());
                    foreach (string s in res)
                        izlaz[izlaz.Count()-1].Add(s);
                    
                }
                else
                {
                    Permute(res, ix + 1, data,izlaz);
                }

            }
        }
        public static List<List<string>> Permute(List<List<string>> data) //mozda cu da samo preimenujem funkcije, posto se bas isto zovu, samo je povratna vrednost drugacija
        {
            List<List<string>> izlaz = new List<List<string>>();
            Permute(new string[data.Count], 0, data,izlaz);
            return izlaz;
        }
    }
}
