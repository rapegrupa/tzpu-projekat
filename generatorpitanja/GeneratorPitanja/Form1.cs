﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;

namespace GeneratorPitanja
{
    public partial class forma_generator : Form
    {
        public List<List<string>> setoviParametara = new List<List<string>>();
        public List<Variranje> parametriZaVariranje = new List<Variranje>(); 
        public List<Promenljiva> promenljive = new List<Promenljiva>();
        public int redniBrojPojaveParametra = 0;//posle cu da ga mozda izmestim negde, za sada je ovde, a mozda ga cak i bolje nazovem
        public List<string> parametriJednogSeta = new List<string>();

        public Dictionary<int, List<string>> recnikParametara = new Dictionary<int, List<string>>();//necu da diram original, nek se
        
        public forma_generator()
        {
            InitializeComponent();
        }

        private void RasklopiPromenljiveUVariranja(List<Promenljiva> promenljive)
        {
            foreach(Promenljiva promenljiva in promenljive)
            {
                int redniBrojParametra = promenljiva.IdUGlavnojListi;
                List<string> vrednostiParametra = new List<string>();
                for (int i = promenljiva.DonjaGranica; i < promenljiva.GornjaGranica; i += promenljiva.Korak)
                    vrednostiParametra.Add(i.ToString());

                this.parametriZaVariranje.Add(new Variranje(redniBrojParametra, vrednostiParametra));//da pokusam kao "hes mapu" da uradim sa insert
                this.recnikParametara.Add(redniBrojParametra, vrednostiParametra);//lookup je jednostavniji uz ovo
            }
        }

        private void dugme_generisi_pitanja_Click(object sender, EventArgs e)
        {
            this.redniBrojPojaveParametra = 0;
           
            this.MojePreuzimanjeParametara();
            this.RasklopiPromenljiveUVariranja(this.promenljive);

            List<List<string>> parametriSaVrednostima = new List<List<string>>();//niz asocijativnih nizova tipa parametar->nizVrednosti na poziciji pojavljivanja parametra
            for(int i=0; i < this.parametriZaVariranje.Count(); i++)
            {
                parametriSaVrednostima.Add(new List<string>());
                foreach(string parametarVariranja in this.parametriZaVariranje[i].ListaParametara) //parametarVariranja su oni <p>-ovi, s tim sto je <p1> npr ceo [], a ne pojedinacni element odatle
                    parametriSaVrednostima[i].Add(parametarVariranja);//parametriSaVrednostima[indeks] <=> <pn>, tj sve odatle, sto znaci da i count moze da se lako izvuce odavde, a i indeksiranje
            }
            //da li ja iznad samo pravim deep kopiju koja ce posle da se permutira?
            //---->>> ovo parametriSaVrednostima se razlikuje od parametriZaVariranje u tipu liste, tj ovo prvo nema idUGlavnojListi kao atribut, vec ima samo vrednosti asocirane za variranje
            //!!! parametri koji su zadati unutar () a da nisu brojacki, su u stvari proste varijacije koje se "ne mesaju" pa onda ni ne idu u parametriZaVariranje, za razliku od brojackih varijacija
            List<List<string>> permutiraneNTorke = Permutiranje.Permute(parametriSaVrednostima);
            List<int> idjeviVarijacije = new List<int>();
            for(int var = 0; var < this.parametriZaVariranje.Count(); var++)
                idjeviVarijacije.Add(this.parametriZaVariranje[var].IdUGlavnojListi);//kad kaze IdUGlavnijListi misli na redni broj ubacene varijiacije mislim. To mi je bitno jer moram da znam sta ide pre necega
            
            
            foreach(List<string> set in this.setoviParametara)
            {
                foreach(List<string> elementNTorke in permutiraneNTorke) //ovo obilazenje je ubacivanje vrednosti kombinacije u parametar za parsiranje
                {
                    List<string> ntorkaVrednostiParametara = new List<string>();
                    if (set == setoviParametara[0])//zasto mi je bitno da ispitam da li obradjujem prvi set?
                    {
                        ntorkaVrednostiParametara = set;
                        for (int k = 0; k < this.parametriZaVariranje.Count(); k++)
                            ntorkaVrednostiParametara[this.parametriZaVariranje[k].IdUGlavnojListi] = elementNTorke[k];//ovde se formira nTorka koja posle ide dole u pravljenju pitanja...
                        //... i to ide na sledeci nacin: prvi iz prvog, drugi iz drugog, itd
                    }
                    else
                    {//ovo je neka obrada kad imam vise setova, al kombinacije mi kao ostaju fiksne, cudno je napisati to, al tipa kombinacije ostaju (evropi,aziji,africi) a pridodavaju se samo varijacije
                        ntorkaVrednostiParametara = setoviParametara[0];
                        for (int k = 0; k < this.parametriZaVariranje.Count(); k++)
                            ntorkaVrednostiParametara[this.parametriZaVariranje[k].IdUGlavnojListi] = elementNTorke[k];
                        
                        int ind = 0;
                        for (int n = 0; n < ntorkaVrednostiParametara.Count(); n++)
                        {
                            if (!idjeviVarijacije.Contains(n))
                            {
                                ntorkaVrednostiParametara[n] = set[ind];
                                ind++;
                            }
                            
                        }
                    }
                    
                    parsiranje(ntorkaVrednostiParametara); //parametriZaParsiranje je ovde niz od n elemenata, pri cemu je prvi neka od vrednosti za prvi parametar, druga isto to za drugi parametar, itd itd 
                    generisanjeOdgovora(ntorkaVrednostiParametara);
                }
            }
            
        }

        #region Bool predikati
        private bool ParametarJeBezModifikacijaPristupa(string tekstZaProveru)
        {
            return (tekstZaProveru.Contains("<p") && !tekstZaProveru.Contains("[") && !tekstZaProveru.Contains("]") && !tekstZaProveru.Contains("<fl"));
        }

        private bool ParametarJeSaIndeksnimPristupom(string tekstZaProveru)
        {
            return (tekstZaProveru.Contains("<p") && tekstZaProveru.Contains("[") && tekstZaProveru.Contains("]"));
        }
        #endregion

        #region Pomocne funkcije za preuzimanje parametara
        private string IzdvojiTekstIzmedjuZagrada(string tekst, string otvorenaZagrada)
        {
            string suprotnaZatvorenaZagrada = "";
            switch(otvorenaZagrada)
            {
                case "(":
                    suprotnaZatvorenaZagrada = ")";
                    break;
                case "[":
                    suprotnaZatvorenaZagrada = "]";
                    break;
                case "{":
                    suprotnaZatvorenaZagrada = "}";
                    break;
                case "<":
                    suprotnaZatvorenaZagrada = ">";
                    break;
                default:
                    break;
            }
            return tekst.Substring(tekst.IndexOf(otvorenaZagrada) + 1, tekst.IndexOf(suprotnaZatvorenaZagrada) - 1 - tekst.IndexOf(otvorenaZagrada));
        }

        private string ObradiVarijaciju(string tekst)
        {
            string parametriUZagradi = this.IzdvojiTekstIzmedjuZagrada(tekst, "(");
            if (parametriUZagradi.Contains("..") && parametriUZagradi.Contains(","))//ovo radim za slucaj da je "brojacka varijacija", znaci postoje dve varijante promenljivih, ako nije brojacka, moram da vidim kako se ubacuje
            {
                string[] petljaIKorak = parametriUZagradi.Split(',');//levo je petlja, desno korak
                string[] donjaIGornjaGranica = petljaIKorak[0].Split(new string[] { ".." }, StringSplitOptions.RemoveEmptyEntries);//moram [2] jer je na [1] iz nekog razloga prazan string
                this.promenljive.Add(new Promenljiva(this.redniBrojPojaveParametra, Int32.Parse(donjaIGornjaGranica[0]), Int32.Parse(donjaIGornjaGranica[1]), Int32.Parse(petljaIKorak[1])));
            }
            this.parametriJednogSeta.Add(parametriUZagradi);//bez obzira na sve, u parametre jednog seta ide ovaj djavo, makar bio sa zarezima, a ako ima zareze, moram da dodam i promenljive
            this.redniBrojPojaveParametra++;
            //rezonovanje iza ovoga je da se parametri svakako dodaju u setu, kakvi god da su, a da se ovo uvecava posto sam naisao na parametar, bio on brojacki ili ne

            return tekst.Substring(parametriUZagradi.Length + 2);
        }

        private string ObradiKombinaciju(string tekst)
        {
            string slepljeniParametriKombinacije = this.IzdvojiTekstIzmedjuZagrada(tekst, "[");
            string[] razdvojeniParametri = slepljeniParametriKombinacije.Split(',');
            List<string> skupVrednostiParametra = new List<string>();//ovo ce da postane "skupVrednostiParametra"
            foreach (string parametar in razdvojeniParametri)
                skupVrednostiParametra.Add(parametar);
            //mozda List<> i nije dobra struktura za cuvanje ovakvih varijacija, izgleda ne moze da se ubaci na proizvoljno mesto ako lista vec nije popunjena tu...
            //...mozda cu da promenim to da bude Dictionary, al onda moram da predefinisem malo preuzimanjeParametara (prvi textBox je tu tekst koji je resurs)
            this.recnikParametara.Add(this.redniBrojPojaveParametra, skupVrednostiParametra);

            this.parametriZaVariranje.Add(new Variranje(this.redniBrojPojaveParametra, skupVrednostiParametra));
            this.redniBrojPojaveParametra++;
            this.parametriJednogSeta.Add(slepljeniParametriKombinacije);

            return tekst.Substring(slepljeniParametriKombinacije.Length + 2);
        }
        #endregion

        public void MojePreuzimanjeParametara()
        {
            this.setoviParametara.Clear();
            this.parametriZaVariranje.Clear();
            this.promenljive.Clear();
            this.redniBrojPojaveParametra = 0;
            this.recnikParametara.Clear();//da sredim onaj glupi bag

            string tekstSaParametrima = this.txt_parametri.Text;
            while(tekstSaParametrima.Length != 0)
            {
                string setParametara = this.IzdvojiTekstIzmedjuZagrada(tekstSaParametrima, "{");
                this.parametriJednogSeta.Clear();
                while(setParametara.Length != 0)
                {
                    if (setParametara[0].Equals('('))
                        setParametara = this.ObradiVarijaciju(setParametara);
                    else
                        setParametara = this.ObradiKombinaciju(setParametara);

                }
                List<string> deepKopijaParametara = new List<string>();
                foreach (string parametar in this.parametriJednogSeta)
                    deepKopijaParametara.Add(parametar);
                this.setoviParametara.Add(deepKopijaParametara);
                tekstSaParametrima = tekstSaParametrima.Substring(tekstSaParametrima.IndexOf("}") + 1);
            }
        }

        #region Pomocne funkcije za parsiranje
        private int IzdvojiIndeksParametraIzTeksta(string tagParametra)
        {
            string ogoljeniParametar = this.IzdvojiTekstIzmedjuZagrada(tagParametra, "<");
            string indeksParametra = ogoljeniParametar.Substring(ogoljeniParametar.IndexOf('p') + 1, ogoljeniParametar.Length - 1);
            
            return Int32.Parse(indeksParametra);
        }

        /*pod monolitnim izrazom podrazumevam <pn>.count, ili samo brojnu vrednost koja se salje unutar []*/
        private int EvaluirajMonolitniIzraz(string ogoljeniTekstIzraza)
        {
            if (!ogoljeniTekstIzraza.Contains("count"))
                return Int32.Parse(ogoljeniTekstIzraza);
            else
            {
                int indeksParametra = this.IzdvojiIndeksParametraIzTeksta(ogoljeniTekstIzraza);
                return this.recnikParametara[indeksParametra].Count;
            }
        }

        private int EvaluirajIndeksniIzraz(string indeksniIzrazTekst)
        {
            /*imam ovde tri slucaja:
             * 1. celobrojna konstanta bez icega
             * 2. izraz tipa <pn>.count takodje bez icega
             * 3. izraz tipa <pn>.count - 4
             * izraz <pn>[4] - 5 necu da smatram validnim
            */
            string ogoljeniTekstIzraza = this.IzdvojiTekstIzmedjuZagrada(indeksniIzrazTekst, "[");
            string[] pojedinacneReciUIzrazu = Regex.Split(ogoljeniTekstIzraza, "-");

            if (pojedinacneReciUIzrazu.Length == 1) //u ovom slucaju definitivno znam da nije treci slucaj
                return this.EvaluirajMonolitniIzraz(ogoljeniTekstIzraza);
            else
            {
                //posto radim sa binarnim izrazima unutar [], a izraz je [prviBroj operator drugiBroj], onda ovo nakon one izolacije ima smisla
                int prviBroj = this.EvaluirajMonolitniIzraz(pojedinacneReciUIzrazu[0]);
                int drugiBroj = this.EvaluirajMonolitniIzraz(pojedinacneReciUIzrazu[2]);
                return prviBroj - drugiBroj;
            }
        }
        #endregion

        public void parsiranje(List<string> ntorkaVrednostiParametara)  //argument - lista parametara, **tj ntorkaVrednostiParametara, gore sam bio napisao neko rezonovanje
        {
            int i = 0;
            
            //fora je sa ovim "lines" sto mozda mogu da unesem vise pitanja u onom textBox-u
            while (i < txt_model_pitanja.Lines.Length)
            {
                string[] pojedinacneReciPitanja = txt_model_pitanja.Lines[i].Split(' ');//pojedinacne reci u modelu pitanja, sa sve parametrima i <>...
                List<string> sablonskoPitanje = new List<string>(); 

                int j = 0;
                while (j < pojedinacneReciPitanja.Count())
                {
                    if (pojedinacneReciPitanja[j].Equals("<for>"))//ako je for u modelu
                    {  
                        string indeksniSimbol = pojedinacneReciPitanja[j + 1].Remove(0, 1);//ovo je da uklonim levu zagradu "("
                        string recSaGranicamaBezZadnjeZagrade = pojedinacneReciPitanja[j + 2].Remove(pojedinacneReciPitanja[j + 2].Length - 1, 1);
                        string[] indeksneGranice = Regex.Split(recSaGranicamaBezZadnjeZagrade, @"\.\.");//moram da escape-ujem tacke, to je any simbol
                        int donjaGranica = this.EvaluirajMonolitniIzraz(indeksneGranice[0]);
                        int gornjaGranica = this.EvaluirajMonolitniIzraz(indeksneGranice[1]);
                        /*ovih pet linija iznad su dovoljne da se evaluira indeksniIzraz (zaglavlje) for petlje (razmotriti mogucnost da ovo postane zasebna funkcija):
                            -pojedinacneReciPitanja[j + 1] : indeksniSimbol sa "(" ispred sebe
                            -pojedinacneRecciPitanja[j + 2] : recSaGranicama, ukljucuje ")" na kraju reci
                            --pre parsiranja moram da uklonim visak zagrada, da escape-ujem .., pa da evaluiram izraz sa onom funkcijom za monolitne izraze
                            --obrada tela for petlje pocinje od pojedinacneReciPitanja[j + 3] valjda, jebem li ga... 
                         * */
                        
                        j += 3;

                        List<string> literaliUnutarFora = new List<string>();
                        bool sadrzajForaJeObradjen = false;
                        while (j < pojedinacneReciPitanja.Count() && !sadrzajForaJeObradjen)
                        {
                            if (!pojedinacneReciPitanja[j].Equals("</for>"))
                            {
                                literaliUnutarFora.Add(pojedinacneReciPitanja[j]);
                                j++;
                            }
                            else
                                sadrzajForaJeObradjen = true;
                        }

                        for (int indeksUForu = donjaGranica; indeksUForu < gornjaGranica; indeksUForu++) //ovo obezbedjuje iteraciju u foru, tj promenu onog indeksa, jer dole samo parsiram stvari unutar for-a
                        {
                            foreach (string literal in literaliUnutarFora) //obilazenje unutar foreach-a nije destruktivno po literale unutar fora, kad se odavde izadje, onda se for aktivira, pa se opet cita sve
                            {
                                if (literal.Contains("<p") && literal.Contains($"[{indeksniSimbol}]")) //indeksni pristup elementu niza, al u obliku <pn>[i]
                                {
                                    int indeks = this.IzdvojiIndeksParametraIzTeksta(literal);                            
                                    string element = ntorkaVrednostiParametara[indeks];
                                    string[] vrednostiZaParametarUNtorci = element.Split(' ');//fora je sa varijacijama sa razmakom je da su odvojene jednim blanko karakterom
                                    sablonskoPitanje.Add(vrednostiZaParametarUNtorci[indeksUForu]);
                                }
                                else
                                {
                                    if (literal.Contains("<p"))
                                    {
                                        int indeks = this.IzdvojiIndeksParametraIzTeksta(literal);
                                        sablonskoPitanje.Add(ntorkaVrednostiParametara[indeks]);
                                    }
                                    else //defaultni slucaj kad literal ili sadrzi <br> (line break), ili jednostavno sadrzi literal bez nekog znacaja, pa se prosto ubaci
                                    {
                                        if (literal.Equals("<br>"))
                                            sablonskoPitanje.Add(Environment.NewLine);
                                        else
                                            sablonskoPitanje.Add(literal);
                                    }
                                }
                            }
                        }
                    }
                    else 
                    {
                        if (this.ParametarJeBezModifikacijaPristupa(pojedinacneReciPitanja[j]))
                        {
                            int indeksParametra = this.IzdvojiIndeksParametraIzTeksta(pojedinacneReciPitanja[j]);
                            sablonskoPitanje.Add(ntorkaVrednostiParametara[indeksParametra]);//<pn> menjam sa konkretnom vrednoscu tog parametra na tom mestu
                            j++;//idem na sledecu rec
                        }       
                        else //ovo je de koji obradjuje rec koja nije <pNesto>, a da ne sadrzi [, tj da li ima nesto sto mora da se obradi u malo slovo
                        {
                            //da li rec koju sad obradjujem sadrzi <pn> u sebi, i da li je taj parametar tipa indeksnog pristupa
                            if (this.ParametarJeSaIndeksnimPristupom(pojedinacneReciPitanja[j]))
                            {
                                int indeksParametra = this.IzdvojiIndeksParametraIzTeksta(pojedinacneReciPitanja[j]);
                                int indeksVrednostiRazradjen = this.EvaluirajIndeksniIzraz(pojedinacneReciPitanja[j]);
                                string trazenaRec = this.recnikParametara[indeksParametra][indeksVrednostiRazradjen];

                                sablonskoPitanje.Add(trazenaRec);
                                j++; //idem dalje sa obradom reci
                            }
                            else //po default-u se rec samo doda na sablonsko pitanje ako uopste nije nijedan specijalni karakter
                            {
                                sablonskoPitanje.Add(pojedinacneReciPitanja[j]);//ovde se dodaju reci koje se nalaze u pitanju
                                j++;
                            }
                        }
                    }
                }

                this.PrikaziPitanjeUTextBoxu(sablonskoPitanje);
                i++;
            }//<<<<-------- ovo je kraj jedne while petlje, nije kraj metode.. sto se tice pitanja, obradjuje se linija po linija, generise se pitanje po pitanje, obradjuje se 
        }

        private void PrikaziPitanjeUTextBoxu(List<string> sablonskoPitanje)
        {
            foreach(string rec in sablonskoPitanje)
            {
                if(rec.Equals(Environment.NewLine))
                    this.txt_generisana_pitanja.Text += rec;
                else
                {
                    if(!rec.Equals("</for>"))
                    {
                        if (rec.Equals("/<for>"))
                            this.txt_generisana_pitanja.Text += "<for> ";
                        else
                        {
                            if (rec.Contains("{") || rec.Contains("}") || rec.Contains("#") || rec.Contains("~") || rec.Contains("=") || rec.Contains(":"))
                                this.obradaSpecijalnihKarakteraZaGift(rec);
                            else
                                this.txt_generisana_pitanja.Text += rec + " ";
                        }
                    }
                }
            }

            this.txt_generisana_pitanja.Text += "\r\n";
        }

        public void konverzijaUMaloSlovo(string tekucaRec, List<string> nizZaStampanje, int k,List<string> listaParametara)
        {
            char prvoSlovo;
            int j = 6;
            string param = "";
            while (!tekucaRec[j].Equals('>'))
            {
                param += tekucaRec[j].ToString();
                j++;
            }
            int indeks = int.Parse(param);
            prvoSlovo = char.ToLower(listaParametara[indeks][0]);
            
            string pom = "";
            int duzina = tekucaRec.Length;
            while (!tekucaRec[duzina - 1].Equals('>'))
            {
                pom += tekucaRec[duzina - 1];
                duzina--;
            }
            char[] pomocni = pom.ToCharArray();
            Array.Reverse(pomocni);
            nizZaStampanje.Add(prvoSlovo.ToString() + new string(pomocni));
        }


        #region Pomocne funkcije za generisanje odgovora
        //predikat koji se poziva sa paroviUslovneJednakosti[1] uglavnom
        private bool KriterijumTacnostiJeNiz(string kriterijumTacnosti)
        {
            return kriterijumTacnosti.Contains("[") && kriterijumTacnosti.Contains("]");
        }

        private bool SplitJeUspeo(string[] potencijalnoSplitovanTekst)
        {
            return potencijalnoSplitovanTekst.Count() == 2;
        }

        private string[] VratiVrednostiIzNiza(string nizVrednosti)
        {
            nizVrednosti = nizVrednosti.Replace("[", "");
            nizVrednosti = nizVrednosti.Replace("]", "");

            return nizVrednosti.Split(',');
        }
        #endregion

        //ovo brate ima 340 i kusur linija...
        //nista, prvo ide preimenovanje, onda ostale szagaimqavtyges...
        public void generisanjeOdgovora(List<string> ntorkaVrednostiParametara)//i ovde je ulazni argument uredjenaNtorkaElemenata
        {
            string koncentrisaniUslovniTekst = txt_tacni_odgovori.Text;
            string[] uslovneKlauzule = koncentrisaniUslovniTekst.Split('\n'); //tacnije, ovo su aj da kazem "formati tacnih odgovora" koje ja naknadno trebam da parsiram i proverim

            //prvo odredim koji je odgovor tacan, a onda stampam sve ponudjene i taj tacan
            string tacanOdgovor = "";
            for (int redniBrojUslovneKlauzule = 0; redniBrojUslovneKlauzule < uslovneKlauzule.Count(); redniBrojUslovneKlauzule++)
            {
                string[] ciniociUslovneKlauzule = Regex.Split(uslovneKlauzule[redniBrojUslovneKlauzule], "<if>");
               
                if (ciniociUslovneKlauzule.Count() > 1)
                {
                    string odgovor = ciniociUslovneKlauzule[0];
                    ciniociUslovneKlauzule[1] = ciniociUslovneKlauzule[1].Trim();
                    string[] usloviPovezaniSaAND = Regex.Split(ciniociUslovneKlauzule[1], "AND");
                    if (usloviPovezaniSaAND.Count() > 1)
                    {
                        bool tacno = true;
                        for (int redniBrojUslova = 0; redniBrojUslova < usloviPovezaniSaAND.Count(); redniBrojUslova++)
                        {
                            string[] ciniociUslovneJednakosti = Regex.Split(usloviPovezaniSaAND[redniBrojUslova], ":="); //ovo testira uslov po uslov u obliku <pn>:=Nesto, s tim sto se ovde menja n, ovaj Split daje <pn> kao prvi element, i vrednost parametra <pn> kao 2.
                            if(this.SplitJeUspeo(ciniociUslovneJednakosti))
                            {
                                int indeksParametraIzUslova = this.IzdvojiIndeksParametraIzTeksta(ciniociUslovneJednakosti[0]);
                                if(this.KriterijumTacnostiJeNiz(ciniociUslovneJednakosti[1]))
                                {                                                   
                                    string[] vrednostiUnutarNiza = this.VratiVrednostiIzNiza(ciniociUslovneJednakosti[1]);
                                    bool postoji = false;//ovo znaci "da li u prosledjenoj ntorciVrednosti postoji parametar koji ispitujem"

                                    for (int redniBrojVrednosti = 0; redniBrojVrednosti < vrednostiUnutarNiza.Count(); redniBrojVrednosti++)
                                    {
                                        string uslovURazmatranju = vrednostiUnutarNiza[redniBrojVrednosti];
                                        
                                        if (ntorkaVrednostiParametara[indeksParametraIzUslova].Equals(uslovURazmatranju))
                                            postoji = true;
                                    }
                                    tacno = tacno && postoji;
                                }
                                else
                                { //ovo se dogadja ako kriterijum tacnosti (on je na poziciji 1) nije zadat kao niz, vec kao jedna jedina vrednost
                                    string uslovURazmatranju = ciniociUslovneJednakosti[1];
                                    
                                    if (!ntorkaVrednostiParametara[indeksParametraIzUslova].Equals(uslovURazmatranju))//sad testiram dal je odgovor tacan, bem ga
                                        tacno = tacno && false;
                                    else
                                        tacno = tacno && true;
                                }
                            }
                            else
                            {
                                //ovo je ako koristim drugi parametar poredjenja NEJEDNAKO ("razlicito", "nije jednako"...)
                                string[] ciniociUslovneRazlicitosti = Regex.Split(usloviPovezaniSaAND[redniBrojUslova], "!=");
                                if(this.SplitJeUspeo(ciniociUslovneRazlicitosti))
                                {
                                    int indeksParametra = this.IzdvojiIndeksParametraIzTeksta(ciniociUslovneJednakosti[0]);
                                    string uslovURazmatranju = ciniociUslovneRazlicitosti[1];
                                    
                                    if (ntorkaVrednostiParametara[indeksParametra].Equals(uslovURazmatranju))
                                        tacno = tacno && false;
                                    else
                                        tacno = tacno && true;
                                }
                            }
                        }
                        if (tacno) //ako se ispitaju svi uslovi, i ako je flag tacnosti tacan, onda znam koji je tacan odgovor, inace idem dalje
                            tacanOdgovor = odgovor;
                    }
                    else
                    {
                        string[] usloviPovezaniSaOR = Regex.Split(ciniociUslovneKlauzule[1], "OR");

                        if (usloviPovezaniSaOR.Count() > 1)  //uslov je sa OR
                        {
                            bool tacno = false;
                            for (int redniBrojUslova = 0; redniBrojUslova < usloviPovezaniSaOR.Count(); redniBrojUslova++) //za svaki uslov se proverava tacnosti, potrebno je da bar jedan bude ispunjen da bi ceo odgovor bio tacan (bem ga)
                            {
                                string[] ciniociUslovneJednakosti = Regex.Split(usloviPovezaniSaOR[redniBrojUslova], ":=");

                                if(this.SplitJeUspeo(ciniociUslovneJednakosti))
                                {
                                    int indeksParametra = this.IzdvojiIndeksParametraIzTeksta(ciniociUslovneJednakosti[0]);

                                    if(this.KriterijumTacnostiJeNiz(ciniociUslovneJednakosti[1]))
                                    {
                                        string[] vrednostiUnutarNiza = this.VratiVrednostiIzNiza(ciniociUslovneJednakosti[1]);
                                        bool postoji = false; //moram da ukapiram sta tacno znaci ovo "postoji"
                                        
                                        for (int redniBrojVrednosti = 0; redniBrojVrednosti < vrednostiUnutarNiza.Count(); redniBrojVrednosti++)
                                        {
                                            string uslovURazmatranju = vrednostiUnutarNiza[redniBrojVrednosti];
                                    
                                            if (ntorkaVrednostiParametara[indeksParametra].Equals(uslovURazmatranju))
                                                postoji = true;
                                        }
                                        tacno = tacno || postoji;//ima smisla jer je ovde OR, gore je AND
                                    }
                                    else //ovo je neki standalone uslov
                                    {
                                        string uslovURazmatranju = ciniociUslovneJednakosti[1];

                                        if (ntorkaVrednostiParametara[indeksParametra].Equals(uslovURazmatranju)) //ako je bar jedan tacan, onda je tacno
                                            tacno = tacno || true;
                                        else
                                            tacno = tacno || false;
                                    }
                                }
                                else  //ovo je OR sa !=
                                {
                                    string[] ciniociUslovneNejednakosti = Regex.Split(usloviPovezaniSaOR[redniBrojUslova], "!=");
                                    if(this.SplitJeUspeo(ciniociUslovneNejednakosti))
                                    {
                                        int indeksParametra = this.IzdvojiIndeksParametraIzTeksta(ciniociUslovneJednakosti[0]);

                                        if (!ntorkaVrednostiParametara[indeksParametra].Equals(ciniociUslovneNejednakosti[1]))
                                            tacno = tacno || true;
                                    }
                                }
                            }

                            if (tacno)
                                tacanOdgovor = odgovor;
                        }
                        else //inace je prosta jednakost (bez AND/OR), STANDALONE uslov, sto znaci da je uslov tipa "Testera" <if> <p0>:="Zahir Sah", znaci ovo je jedan uslov (izgleda?)
                        {
                            string[] ciniociUslovneJednakosti = Regex.Split(usloviPovezaniSaOR[0], ":="); //usloviPovezaniSaOR, al ovde je to jedan jedini uslov (ili nijedan koliko vidim) zbog if-a gore
                            if(this.SplitJeUspeo(ciniociUslovneJednakosti))
                            {
                                int indeksParametra = this.IzdvojiIndeksParametraIzTeksta(ciniociUslovneJednakosti[0]);//ovo moze da zameni 6 linija dole po mojoj proceni

                                if(this.KriterijumTacnostiJeNiz(ciniociUslovneJednakosti[1]))
                                {
                                    string[] vrednostiUnutarNiza = this.VratiVrednostiIzNiza(ciniociUslovneJednakosti[1]);
                                    for (int redniBrojVrednosti = 0; redniBrojVrednosti < vrednostiUnutarNiza.Count(); redniBrojVrednosti++)
                                    {
                                        string uslovURazmatranju = vrednostiUnutarNiza[redniBrojVrednosti];
                                        
                                        if (ntorkaVrednostiParametara[indeksParametra].Equals(uslovURazmatranju))
                                            tacanOdgovor = odgovor;
                                    }
                                }
                                else
                                {
                                    string uslovURazmatranju = ciniociUslovneJednakosti[1];
                                    if (ntorkaVrednostiParametara[indeksParametra].Equals(uslovURazmatranju))
                                        tacanOdgovor = odgovor;
                                }
                            }
                            else //---->>> ova grana je grana za prost uslov bez AND ili OR i sa znakom != umesto :=
                            {
                                string[] ciniociUslovneNejednakosti = Regex.Split(usloviPovezaniSaOR[0], "!=");
                                int indeksParametra = this.IzdvojiIndeksParametraIzTeksta(ciniociUslovneNejednakosti[0]);

                                if(this.KriterijumTacnostiJeNiz(ciniociUslovneNejednakosti[1]))
                                {
                                    string[] vrednostiUnutarNiza = this.VratiVrednostiIzNiza(ciniociUslovneJednakosti[1]);
                                    bool postoji = false;

                                    for (int redniBrojVrednosti = 0; redniBrojVrednosti < vrednostiUnutarNiza.Count(); redniBrojVrednosti++)
                                    {
                                        string uslovURazmatranju = vrednostiUnutarNiza[redniBrojVrednosti];
                                        
                                        if (ntorkaVrednostiParametara[indeksParametra].Equals(uslovURazmatranju)) //ako se bar jedan poklapa, onda je false
                                            postoji = true;
                                    }
                                    if (!postoji)
                                        tacanOdgovor = odgovor;
                                }
                                else
                                {
                                    string uslovURazmatranju = ciniociUslovneNejednakosti[1];
                                    
                                    if (!ntorkaVrednostiParametara[indeksParametra].Equals(uslovURazmatranju))
                                        tacanOdgovor = odgovor;
                                }
                            }
                        }
                    }
                }
            }
            
            //ovako na prvu loptu bez ikakvog tumacenja koda niti analize, kapiram da je ovaj deo zaduzen za ispisivanje odgovora u neki textBox
            txt_generisana_pitanja.Text += "{" + Environment.NewLine;
            List<string> ponudjeniOdgovori = new List<string>(); //maksimalno 5 ponudjenih odgovora, al pazi sad, ovaj je prosledjivao celokupne uslovne klauzule, moram da vidim sto...
            if (uslovneKlauzule.Count() < 5)
            {
                for (int i = 0; i < uslovneKlauzule.Count(); i++)
                    ponudjeniOdgovori.Add(uslovneKlauzule[i]);
            }
            else
            {
                int a, b, c, e = 0;
                List<int> brojevi = new List<int>();
                for (int i = 0; i < uslovneKlauzule.Count(); i++)
                {
                    if (uslovneKlauzule[i].Contains(tacanOdgovor))
                    {
                        brojevi.Add(i);
                        e = i;
                    }
                }
                Random rand = new Random();
                do
                {
                    a = rand.Next(1, uslovneKlauzule.Count()) - 1;
                    b = rand.Next(1, uslovneKlauzule.Count()) - 1;
                    c = rand.Next(1, uslovneKlauzule.Count()) - 1;
                    /*a--;
                    b--;
                    c--;*/
                } while ((a == b) || (a == e) || (b == e) || (c == e) || (c == a) || (c == b));
                brojevi.Add(a);
                brojevi.Add(b);
                brojevi.Add(c);

                for (int i = 0; i < uslovneKlauzule.Count(); i++)
                {
                    if (brojevi.Contains(i))
                        ponudjeniOdgovori.Add(uslovneKlauzule[i]);
                }
            }
            int redniBrojTacnog = -1;

            for (int i = 0; i < ponudjeniOdgovori.Count(); i++)
            {
                int indexOfFirstOcc = ponudjeniOdgovori[i].IndexOf("<if>");
                int ind = 0;
                string ponudjeniOdgovor = "";
                while(ind < indexOfFirstOcc && ind != -1)
                {
                    ponudjeniOdgovor += ponudjeniOdgovori[i][ind].ToString();
                    ind++;
                }
                
                if (ponudjeniOdgovor.Equals(tacanOdgovor)) 
                    redniBrojTacnog = i + 1;
                
            }
            
            for(int k = 0; k < ponudjeniOdgovori.Count; k++)
            {
                if(k == redniBrojTacnog - 1 && redniBrojTacnog != -1)
                    this.txt_generisana_pitanja.Text += "=";
                else
                    this.txt_generisana_pitanja.Text += "~";
                
                
                if (!ponudjeniOdgovori[k].Contains("<if>"))
                {
                    string[] nizReci = ponudjeniOdgovori[k].Split(' ');
                    foreach (string rec in nizReci)
                        this.obradaReciUOdgovoru(ntorkaVrednostiParametara, rec);

                    this.txt_generisana_pitanja.Text += Environment.NewLine;
                }
                else
                {
                    string[] nizReci = ponudjeniOdgovori[k].Split(' ');
                    int count = nizReci.Count();
                    int i = 0;

                    while (!nizReci[i].Equals("<if>") && i < count - 1) //brate uradis Regex.Split("<if>") pa posaljes prvu rec...
                    {
                        this.obradaReciUOdgovoru(ntorkaVrednostiParametara, nizReci[i]);
                        i++;
                    }
                    txt_generisana_pitanja.Text += Environment.NewLine;
                }
            }

            txt_generisana_pitanja.Text += "}" + Environment.NewLine;
            txt_generisana_pitanja.Text += Environment.NewLine;
            
        }

        public void obradaReciUOdgovoru(List<string> listaParametara, string rec)
        {
            if (!rec.Contains("<") && !rec.Contains(">"))
                this.txt_generisana_pitanja.Text += rec + " ";
            else  //znaci da ima parametri
            {
                if (rec.Contains("<fl>") && rec.Contains("</fl>"))  //ako ima ovo
                {
                    int p = 6; //odatle pocinje redni broj parametra
                    string pom = "";
                    while (!rec[p].Equals('>'))
                    {
                        pom += rec[p].ToString();
                        p++;
                    }
                    int rednibrojParametra = int.Parse(pom);
                    char prvoSlovo = listaParametara[rednibrojParametra][0];
                    p += 6;
                    string pom1 = "";
                    while (p < rec.Length)
                    {
                        pom1 += rec[p].ToString();
                        p++;
                    }
                    this.txt_generisana_pitanja.Text += char.ToLower(prvoSlovo).ToString() + pom1 + " ";
                }
                else //inace je samo parametar
                {
                    int p = 2;
                    string pom = "";
                    while (!rec[p].Equals('>'))
                    {
                        pom += rec[p].ToString();
                        p++;
                    }
                    int rednibrojParametra = int.Parse(pom);
                    txt_generisana_pitanja.Text += listaParametara[rednibrojParametra] + " ";
                }
            }
        }

        private void ModifikujRec(string prosledjenaRec, char prosledjeniKarakter)
        {
            int pozicijaKaraktera = prosledjenaRec.IndexOf(prosledjeniKarakter);
            string novaRec = prosledjenaRec.Insert(pozicijaKaraktera, @"\");
            this.txt_generisana_pitanja.Text += novaRec + " ";
        }

        public void obradaSpecijalnihKarakteraZaGift(string rec)
        {
            if (rec.Contains("{"))
                this.ModifikujRec(rec, '{');
            if (rec.Contains("}"))
                this.ModifikujRec(rec, '}');
            if (rec.Contains("#"))
                this.ModifikujRec(rec, '#');
            if (rec.Contains("~"))
                this.ModifikujRec(rec, ':');
            if (rec.Contains('='))
                this.ModifikujRec(rec, '=');
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void dugme_obrisi_generisana_pitanja_Click(object sender, EventArgs e)
        {
            txt_generisana_pitanja.Clear();
        }

        private void dugme_snimi_Click(object sender, EventArgs e)
        {
            string putanjaFoldera = Directory.GetCurrentDirectory() + "/Rezultati/";
            DirectoryInfo d = new DirectoryInfo(putanjaFoldera);
            FileInfo[] fajlovi = d.GetFiles("*");
            int[] naziviFajlova = new int[200];
            for(int i=0;i<fajlovi.Count();i++)
            {
                naziviFajlova[i] = int.Parse(fajlovi[i].Name);
            }
            int nazivNovogFajla;
            if(naziviFajlova.Count() > 0)
            {
                nazivNovogFajla = naziviFajlova.Max() + 1;
            }
            else
            {
                nazivNovogFajla = 1;
            }
            string putanjaFajla = putanjaFoldera + nazivNovogFajla.ToString();

            using (StreamWriter streamWriter = new StreamWriter(putanjaFajla))
            {
                int j = 0;

                while (j < txt_generisana_pitanja.Lines.Length)
                {
                    streamWriter.WriteLine(txt_generisana_pitanja.Lines[j]);
                    j++;
                }
                MessageBox.Show("Rezultati su uspesno snimljeni!");
            }
        }
    }
}
