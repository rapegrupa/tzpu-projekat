﻿namespace GeneratorPitanja
{
    partial class forma_generator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_model_pitanja = new System.Windows.Forms.TextBox();
            this.dugme_generisi_pitanja = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_parametri = new System.Windows.Forms.TextBox();
            this.txt_tacni_odgovori = new System.Windows.Forms.TextBox();
            this.txt_generisana_pitanja = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dugme_obrisi_generisana_pitanja = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dugme_snimi = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txt_model_pitanja
            // 
            this.txt_model_pitanja.Location = new System.Drawing.Point(370, 46);
            this.txt_model_pitanja.Margin = new System.Windows.Forms.Padding(2);
            this.txt_model_pitanja.Multiline = true;
            this.txt_model_pitanja.Name = "txt_model_pitanja";
            this.txt_model_pitanja.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_model_pitanja.Size = new System.Drawing.Size(319, 305);
            this.txt_model_pitanja.TabIndex = 0;
            // 
            // dugme_generisi_pitanja
            // 
            this.dugme_generisi_pitanja.Location = new System.Drawing.Point(362, 384);
            this.dugme_generisi_pitanja.Margin = new System.Windows.Forms.Padding(2);
            this.dugme_generisi_pitanja.Name = "dugme_generisi_pitanja";
            this.dugme_generisi_pitanja.Size = new System.Drawing.Size(208, 43);
            this.dugme_generisi_pitanja.TabIndex = 1;
            this.dugme_generisi_pitanja.Text = "Generiši pitanja";
            this.dugme_generisi_pitanja.UseVisualStyleBackColor = true;
            this.dugme_generisi_pitanja.Click += new System.EventHandler(this.dugme_generisi_pitanja_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 19);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Parametri:";
            // 
            // txt_parametri
            // 
            this.txt_parametri.Location = new System.Drawing.Point(74, 16);
            this.txt_parametri.Margin = new System.Windows.Forms.Padding(2);
            this.txt_parametri.Multiline = true;
            this.txt_parametri.Name = "txt_parametri";
            this.txt_parametri.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_parametri.Size = new System.Drawing.Size(217, 62);
            this.txt_parametri.TabIndex = 3;
            // 
            // txt_tacni_odgovori
            // 
            this.txt_tacni_odgovori.Location = new System.Drawing.Point(20, 118);
            this.txt_tacni_odgovori.Margin = new System.Windows.Forms.Padding(2);
            this.txt_tacni_odgovori.Multiline = true;
            this.txt_tacni_odgovori.Name = "txt_tacni_odgovori";
            this.txt_tacni_odgovori.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_tacni_odgovori.Size = new System.Drawing.Size(312, 254);
            this.txt_tacni_odgovori.TabIndex = 4;
            // 
            // txt_generisana_pitanja
            // 
            this.txt_generisana_pitanja.Location = new System.Drawing.Point(728, 21);
            this.txt_generisana_pitanja.Margin = new System.Windows.Forms.Padding(2);
            this.txt_generisana_pitanja.Multiline = true;
            this.txt_generisana_pitanja.Name = "txt_generisana_pitanja";
            this.txt_generisana_pitanja.ReadOnly = true;
            this.txt_generisana_pitanja.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_generisana_pitanja.Size = new System.Drawing.Size(348, 491);
            this.txt_generisana_pitanja.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 94);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Odgovori:";
            // 
            // dugme_obrisi_generisana_pitanja
            // 
            this.dugme_obrisi_generisana_pitanja.Location = new System.Drawing.Point(362, 432);
            this.dugme_obrisi_generisana_pitanja.Margin = new System.Windows.Forms.Padding(2);
            this.dugme_obrisi_generisana_pitanja.Name = "dugme_obrisi_generisana_pitanja";
            this.dugme_obrisi_generisana_pitanja.Size = new System.Drawing.Size(208, 49);
            this.dugme_obrisi_generisana_pitanja.TabIndex = 7;
            this.dugme_obrisi_generisana_pitanja.Text = "Obriši generisana pitanja";
            this.dugme_obrisi_generisana_pitanja.UseVisualStyleBackColor = true;
            this.dugme_obrisi_generisana_pitanja.Click += new System.EventHandler(this.dugme_obrisi_generisana_pitanja_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(368, 30);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Šablonsko pitanje:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(675, 21);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Rezultat:";
            // 
            // dugme_snimi
            // 
            this.dugme_snimi.Location = new System.Drawing.Point(17, 384);
            this.dugme_snimi.Margin = new System.Windows.Forms.Padding(2);
            this.dugme_snimi.Name = "dugme_snimi";
            this.dugme_snimi.Size = new System.Drawing.Size(171, 51);
            this.dugme_snimi.TabIndex = 10;
            this.dugme_snimi.Text = "Snimi rezultate";
            this.dugme_snimi.UseVisualStyleBackColor = true;
            this.dugme_snimi.Click += new System.EventHandler(this.dugme_snimi_Click);
            // 
            // forma_generator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1097, 531);
            this.Controls.Add(this.dugme_snimi);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dugme_obrisi_generisana_pitanja);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_generisana_pitanja);
            this.Controls.Add(this.txt_tacni_odgovori);
            this.Controls.Add(this.txt_parametri);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dugme_generisi_pitanja);
            this.Controls.Add(this.txt_model_pitanja);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "forma_generator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Generator sablonski pitanja zatvorenog tipa za testiranje visih nivoa ucenja";
            this.TransparencyKey = System.Drawing.Color.Gray;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_model_pitanja;
        private System.Windows.Forms.Button dugme_generisi_pitanja;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_parametri;
        private System.Windows.Forms.TextBox txt_tacni_odgovori;
        private System.Windows.Forms.TextBox txt_generisana_pitanja;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button dugme_obrisi_generisana_pitanja;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button dugme_snimi;
    }
}

