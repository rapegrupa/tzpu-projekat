﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorPitanja
{
    public class Variranje
    {
        public int IdUGlavnojListi { get; set; }  //ovo je pojavni redni broj, konkretno ovde za kombinaciju []
        public List<string> ListaParametara { get; set; } //pojedinacne vrednosti unutar parametra kombinacije

        public Variranje()
        {
            this.ListaParametara = new List<string>();
        }
        
        public Variranje(int IDUGlavnojListi, List<string> listaParametara)
        {
            this.IdUGlavnojListi = IDUGlavnojListi;
            this.ListaParametara = listaParametara;
        }

    }
}
